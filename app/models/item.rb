class Item < ApplicationRecord
    
    validates :name, presence: true, length: { maximum: 50 }
    validates :price, presence: true
    validate :integer?
    
    def integer?
        unless price.to_i.positive?
            errors.add(:price, "should be a valid number")
        end
    end
    
    def self.visible
        where(hidden: false)
    end
end