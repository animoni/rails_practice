require 'test_helper'

class UserTest < ActiveSupport::TestCase
    
    def setup
       @user = User.new(username: "test_user") 
    end
    
    test 'user should be valid' do
        assert @user.valid?
    end
    
    test 'username should be present valid' do
       user = User.new(username: "   ")
       assert_not user.valid?
    end
    
    test "username should not be too long" do
        @user.username = "a" * 26
        assert_not @user.valid?
    end

end
