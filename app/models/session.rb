class Session
    include ActiveModel::Model
    
    attr_accessor :username
    
    validates :username, presence: true
    validate :username_exist_in_database
    
   def username_exist_in_database
        unless User.find_by(username: username)
           errors.add(:username, "user doesn't exist")
        end
        
   end
end
