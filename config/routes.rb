Rails.application.routes.draw do
  get 'sessions/new'

   root 'static_pages#home'
   resources :items
   resources :users
    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
   
   resources :basket_items, only: [:create, :index, :destroy] 
   
   get '/basket', to: 'basket_items#index'
   
   resources :quotes, only: [:index]
end
