require 'test_helper'

class LoginUserTest < ActionDispatch::IntegrationTest

  def setup
    @user = User.create(username: "test_user")
  
  end
  
  test 'successful login' do
    get login_path
    assert_template 'sessions/new'
    assert_select 'h1', test: 'Login'

    # login(username) => user show page && user saved in session
    post login_path params: { session: { username: "test_user" } }
    # does the website render the correct resulting page
    # is the username saved in the session

    assert is_logged_in?
    assert_redirected_to @user
  end
  
  test 'unsuccessful login with empty username' do
    post login_path params: { session: { username: "" } }
    
    assert_template 'sessions/new'
    assert_select 'div.alert', 'The form contains 2 errors.'
    # should show the validation messages
  end
  
  # class Website
  #   attr_reader :session
    
  #   def initialize
  #     @session = {}
  #   end
    
  #   def login(username)
  #     @session[:username] = username
  #     true
  #   end
    
  #   def logged_in?
  #     @session[:username].present?
  #   end
  # end
  
  # website = Website.new
  # result = website.login('test_user')
  # expect(result).to eq(true)
  # expect(website.session[:username]).to eq('test_user')
  # expect(website.logged_in?).to eq(true)
  
end