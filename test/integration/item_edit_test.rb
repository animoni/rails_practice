require 'test_helper'

class ItemEditTest < ActionDispatch::IntegrationTest
    
  test 'invalid price update information' do
	item = Item.create(name: 'Test', price: 12)
	
	put item_path(item), params: {item: {price: 'Test'}}
	
	assert_template 'items/edit'
	assert_select '.alert-danger', text: 'Invalid information.' 
  end
    

end