require 'test_helper'

class CreateNewItemTest < ActionDispatch::IntegrationTest
    

    test 'the item is saved into the database and displayd on the item show page' do
        get new_item_path
        assert_template 'items/new'
        assert_difference 'Item.count', 1 do 
            post items_path, params: { item: {name: "Test", price: 12}}
        end
        follow_redirect!
        assert_template 'items/show'
        assert_select 'a[href=?]', items_path
    end
    
end