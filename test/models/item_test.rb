require 'test_helper'

class ItemTest < ActiveSupport::TestCase
    
  def setup
      @item = Item.new(name: "Product", price: 12, hidden: "", add_to_basket: "")
  end
    
  test 'item should be valid' do
    assert @item.valid?
  end
  
  test 'item should not be valid' do
    @item.price = 'foo'
    assert_not @item.valid?
  end
  
  test 'item\'s price should not be 0' do
    @item.price = 0
    assert_not @item.valid?
  end
  
  test 'item\'s price should not be negative number' do
    @item.price = -12
    assert_not @item.valid?
  end
  
  test 'item should be saved with valid information ' do 
    @item.save!
    assert @item.valid?
  end
  
  test 'item should not be saved with blank name' do
    item = Item.new(name:"", price: 12, hidden: "")
    item.save
    assert_not item.valid?
  end
  
end
