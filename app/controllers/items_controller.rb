class ItemsController < ApplicationController
    def index
        @items = Item.visible
        
    end
    
    def new
        @item = Item.new
    end
    
    def create
        @item = Item.new(item_params)
        if @item.save
            redirect_to @item
           # flash[:success] = "New item created."
        else
           # flash[:danger] = "Name can't be blank"
            render 'new'
        end
    end
    
    def show
       @item = find_item
    end
    
    def edit 
        @item = find_item
    end
    
    def update
        @item = find_item
        if @item.update_attributes(item_params)
            @item.save#it can update individually the params
            flash.now[:info] = "Item updated."
            redirect_to @item
        else
            flash.now[:danger] = "Invalid information."
            render 'edit'
        end
    end

    
    
    def hide
        @item = find_item
        @item.destroy
        redirect_to items_url
    end
    
    def delete 
       @item = find_item 
       @item.delete
        
    end


    private
        def item_params
            params.require(:item).permit(:name, :price, :hidden, :add_to_basket)
        end
        
        def find_item
           Item.visible.find(params[:id] ) 
        end
        
        def item_message # Why did this not work in the helper?
             "Price for the new item is #{@item.price}"
        end
    
end