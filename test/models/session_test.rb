require 'test_helper'

class SessionTest < ActiveSupport::TestCase
    test "it validates that username is always present" do
      session_form = Session.new(username: '')
      
      session_form.valid?
     # binding.pry
      assert session_form.errors.messages[:username] == ["can't be blank", "user doesn't exist"]
    end
    
    test 'when the username is present it won\' return any error' do
      session_form = Session.new(username: "bla")
      
      session_form.valid?
      
      assert session_form.errors.messages[:username] == ["user doesn't exist"]
      
    end
    
    test 'when the username is not blank and user exist in db' do 
      user = User.create(username: "robin")
      
      session_form = Session.new(username: "robin")
      
      assert session_form.valid? == true
    end
end
