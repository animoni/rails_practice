class BasketItemsController < ApplicationController
    
  def index
    if session[:items_in_basket].nil?
      flash[:danger] = "Basket is empty."
      redirect_to root_url
    else
      @basket_items = Item.find(session[:items_in_basket])
      @basket_items ||= Array.new
    end
    
   
    
  end
    
  def create
    item_id = params[:item_id]
    @item = Item.find(item_id)
    @items_in_basket = session[:items_in_basket]
    @items_in_basket ||= Array.new
    @items_in_basket.push(@item.id)
    session[:items_in_basket] = @items_in_basket
    flash[:info] = "Item added to basket."
    
  end
    
  def destroy 
    reset_session
    flash[:danger] = "Basket empty."
    redirect_to root_url
  end
    
  
    
end
